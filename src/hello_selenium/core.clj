;; Project dependencies:
;; [org.seleniumhq.selenium/selenium-chrome-driver "3.141.59"]
;; [org.seleniumhq.selenium/selenium-java "3.141.59"]

(ns hello-selenium.core
  (:gen-class)
  (:import [org.openqa.selenium By WebDriver WebElement]
           [org.openqa.selenium.chrome ChromeDriver]))

(defn -main []
  (let [driver (ChromeDriver.)]
    (.get driver "http://www.google.com/")
    (let [e (By/name "q")
          search-box (.findElement driver e)]
      (.sendKeys search-box (into-array ["getting started with Selenium"]))
      (.submit search-box)
      (Thread/sleep 5000)
      (.quit driver))))
